import { createMuiTheme } from "@material-ui/core/styles";

const useMuiTheme = () => {
  return(
    createMuiTheme({
      palette: {
        primary: {
          main: '#0F4780'
        },
        secondary: {
          main: '#6B64FF'
        },
        error: {
          main: "#de2323"
        }
      },
      breakpoints: {
        values: {
          sm: 700
        }
      },
      overrides: {
        MuiButton: {
          root: {
            width: '100%',
            padding: '10px 16px',
            fontSize: '16px',
            fontWeight: "bold"
          },
          containedSecondary: {
            "&&:hover": {
              backgroundColor: "transparent",
              color: "#6B64FF",
              boxShadow: "none"
            }
          }
        },
      }
    }
    )
  )
};

export {useMuiTheme}