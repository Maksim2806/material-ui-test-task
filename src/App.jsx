import CssBaseline from '@material-ui/core/CssBaseline';
import {  Box, Grid } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { useMuiTheme } from './theme/theme';
import { Button } from './components/Button';
import { Card } from './components/Card';

const App = () => {
  const theme = useMuiTheme()
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline/>        
          <Box width="100%" p={4}>
            <Grid container justify="center">
              <Card>
                <Grid 
                  container
                  justify="space-between"
                  alignItems="center"
                  spacing={2}
                  >
                    <Grid item xs={6} sm={12}>
                      <Button error={true} variant="contained" color="secondary">Error</Button>
                    </Grid>
                    <Grid item xs={6} sm={12}>
                      <Button variant="contained" color="secondary">Button 1</Button>
                    </Grid>
                    <Grid item xs={6} sm={12}>
                      <Button variant="contained" color="primary">Button 2</Button>
                    </Grid>
                    <Grid item xs={6} sm={12}>
                      <Button variant="text" color="secondary">Button 3</Button>
                    </Grid>
                </Grid>
              </Card>
            </Grid>
          </Box>    
    </ThemeProvider>
  );
}

export default App;