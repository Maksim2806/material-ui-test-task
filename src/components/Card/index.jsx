import React from 'react'
import { Paper } from '@material-ui/core';
import { useStyles } from './styles';

export const Card = ({children, ...rest}) => {
    const classes = useStyles()
    return (
        <Paper {...rest} className={classes.root}>
            {children}   
        </Paper>
    )
}
