import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => {
   return {
    root: {
        width: "400px",
        padding: "20px",
        "&.MuiPaper-rounded": {
          borderRadius: "8px"
        }
      }
   }
})