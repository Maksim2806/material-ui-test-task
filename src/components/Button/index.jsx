import React from 'react'
import { useStyles } from './styles'
import { Button as MuiButton } from '@material-ui/core'

export const Button = ({children, ...rest}) => {
    const classes = useStyles(rest)
    const {error, variant} = rest
    return (
        <MuiButton {...rest} className={classes.root} variant={error ? 'contained' : variant}>{children}</MuiButton>
    )
}