import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: ({error, color}) => error ? theme.palette.error.main : color || theme.palette.primary.main
    }
}))